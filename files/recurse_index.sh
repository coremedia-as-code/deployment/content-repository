#!/bin/bash

set -e

pushd $1 > /dev/null

for i in $(ls -d */)
do
	pushd $i > /dev/null
        echo "" > checksums
	tree -H '.' -I *.html -s -L 1 --noreport --charset utf-8 > index.html
        for i in $(ls -1 *.jar 2> /dev/null)
        do
		sha512sum $i >> checksums
	done
        for i in $(ls -1 *.zip 2> /dev/null)
	do
		sha512sum $i >> checksums
	done
	popd > /dev/null
done

tree -a -H '.' -I *.html -s --noreport --charset utf-8 > index.html

popd > /dev/null

