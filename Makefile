export GIT_SHA1          := $(shell git rev-parse --short HEAD)
export DOCKER_IMAGE_NAME := nginx
export DOCKER_NAME_SPACE := ${USER}
export DOCKER_VERSION    ?= latest

.PHONY: build shell run exec start stop clean

default: build

build:
	@hooks/build

shell:
	@hooks/shell

run:
	@hooks/run

exec:
	@hooks/exec

start:
	@hooks/start

stop:
	@hooks/stop

clean:
	@hooks/clean
