# CoreMedia - content-repository

Docker container with an running nginx and available CoreMedia Artefacts.

## usage

```
$ mkdir files/1904.1
$ cp *.war files/1904.1/
$ cp {cms,mls,rls}.zip files/1904.1/
$ make
$ docker ps
CONTAINER ID        IMAGE                       COMMAND                  CREATED              STATUS              PORTS                                      NAMES
2d74691cb065        bodsch/nginx:latest         "nginx -g 'daemon of…"   About a minute ago   Up 59 seconds       0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   bodsch-nginx

$ curl localhost/
<html>
  <head></head>
  <body>
    <div style="font-size:1.2em;text-align:center">CoreMedia Content-Server</div>



  </body>
</html>

$ curl -I  localhost/1904.1/content-feeder.war

$ make stop
$ make clean
```
## create index.html

```
$ cd files/1904.1
$ tree -H '.' -L 1 --noreport --charset utf-8 > index.html
```

# Ports

 * 80
 * 443
